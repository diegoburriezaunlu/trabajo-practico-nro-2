package ar.edu.unlu.poo.tp2.ejercicio7;

public class Producto {
    private String descripcion;
    private float valorUnitario;
    private String codigoInterno;
    public Producto(String descripcion,float valorUnitario,String codigoInterno){
        this.descripcion=descripcion;
        this.valorUnitario=valorUnitario;
        this.codigoInterno=codigoInterno;
    }
    String getDescripcion(){
        return this.descripcion;
    }
    float getValorUnitario(){
        return this.valorUnitario;
    }
    String getCodigoInterno(){
        return this.codigoInterno;
    }
}
