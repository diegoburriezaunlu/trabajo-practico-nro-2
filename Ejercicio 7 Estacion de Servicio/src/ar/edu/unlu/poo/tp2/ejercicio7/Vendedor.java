package ar.edu.unlu.poo.tp2.ejercicio7;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;

public class Vendedor {
    private String nombre;
    private String codigoInterno;
    private LocalDate beneficioDescuento;
    private ArrayList<AcumuladorVenta> ventasMensuales = new ArrayList<>();
    public Vendedor(String nombre,String codigo){
        this.nombre = nombre;
        this.codigoInterno = codigo;
        this.beneficioDescuento =LocalDate.now();
    }

    public LocalDate getBeneficioDescuento() {
        return beneficioDescuento;
    }

    public void setBeneficioDescuento(LocalDate beneficioDescuento) {
        this.beneficioDescuento = beneficioDescuento;
    }

    String getNombre(){
        return this.nombre;
    }
    String getCodigoInterno(){
        return this.codigoInterno;
    }
    private AcumuladorVenta buscarMesDeVenta(Month mes,Year anio){
        AcumuladorVenta respuesta = null;
        for(int i=0;i<ventasMensuales.size();i++){
            if (ventasMensuales.get(i).getmes().equals(mes)&&ventasMensuales.get(i).getAnio().equals(anio)){
                respuesta = ventasMensuales.get(i);
            }
        }
        if (respuesta==null){
            respuesta = new AcumuladorVenta(LocalDate.now());
        }
        return respuesta;
    }
    void cargarVenta(LocalDate fecha,double monto){
        Month mes = Month.from(fecha);
        Year anio = Year.from(fecha);
        buscarMesDeVenta(mes,anio).sumarValor(monto);
    }
    double getMontoTotalVentas(Month mes, Year anio){
        double res=0;
        for(int i=0;i<ventasMensuales.size();i++){
            if (ventasMensuales.get(i).getmes().equals(mes)&&ventasMensuales.get(i).getAnio().equals(anio)){
                res = ventasMensuales.get(i).getTotal();
            }
        }
        return res;
    }
}
