package ar.edu.unlu.poo.tp2.ejercicio7;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

public class AcumuladorVenta {
    private LocalDate mes;
    private double total;

    public AcumuladorVenta(LocalDate mes) {
        this.mes = mes;
        this.total = 0;
    }

    public Year getAnio() {
        return Year.from(this.mes);
    }
    public Month getmes(){
        return Month.from(this.mes);
    }

    public void setMes(LocalDate fecha) {
        this.mes = fecha;
    }

    public double getTotal() {
        return total;
    }

    public void sumarValor(double total) {
        this.total += total;
    }
}
