package ar.edu.unlu.poo.tp2.ejercicio7;

import java.time.LocalDate;

public class Venta {
    private Cliente cliente;
    private Producto producto;
    private Double totalVenta;
    private Vendedor vendedor;
    private float litros;
    private Expendedor expendedor;
    private LocalDate fecha;

    public Venta(Cliente cliente,Producto producto,Vendedor vendedor,Float litros,Expendedor expendedor){
        this.cliente = cliente;
        this.producto = producto;
        this.vendedor = vendedor;
        this.litros = litros;
        this.fecha = LocalDate.now();
        this.totalVenta = (double)(litros* producto.getValorUnitario());
        this.expendedor = expendedor;
        expendedor.setVentaTotal(producto.getValorUnitario()*litros);
        cliente.cargarCompra(LocalDate.now(),this.totalVenta);
        vendedor.cargarVenta(LocalDate.now(),this.totalVenta);
    }
    public Cliente getCliente() {
        return cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public Double getTotalVenta() {
        return totalVenta;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public float getLitros() {
        return litros;
    }

    public LocalDate getFecha() {
        return fecha;
    }

}
