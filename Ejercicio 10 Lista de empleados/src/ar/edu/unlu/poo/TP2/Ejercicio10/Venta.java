package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;

public class Venta {
    private LocalDate fecha;
    private double monto;

    public Venta(LocalDate fecha, double monto) {
        this.fecha = fecha;
        this.monto = monto;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public double getMonto() {
        return monto;
    }
}
