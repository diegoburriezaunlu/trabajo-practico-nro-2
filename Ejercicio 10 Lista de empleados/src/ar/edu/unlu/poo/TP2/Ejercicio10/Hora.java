package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;

public class Hora {
    private LocalDate fecha;
    private int horas;

    public Hora(LocalDate fecha, int horas) {
        this.fecha = fecha;
        this.horas = horas;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public int getHoras() {
        return horas;
    }
}
