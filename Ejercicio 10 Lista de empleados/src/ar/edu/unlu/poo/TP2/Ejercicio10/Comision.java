package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

public class Comision extends Empleado {
    private ArrayList<Venta> ventas = new ArrayList<>();
    private float porcentaje;

    public Comision(String nombre, String apellido, String telefono,
                    String cuit, LocalDate fechaDeCumpleanios, float porcentaje) {
        super(nombre, apellido, telefono, cuit, fechaDeCumpleanios);
        this.porcentaje = porcentaje;
    }
    public double calcularSueldoClase(Month mes){
        double sumaVentas =0;
        for (int i =0 ;i<ventas.size();i++){
            if (Month.from(ventas.get(i).getFecha()).equals(mes)){
                sumaVentas += ventas.get(i).getMonto();
            }
        }
        return sumaVentas * (getPorcentaje()/100);
    }

    @Override
    public double calculoPorCumpleanios(double monto) {
        Month mes = Month.from(super.getFechaDeCumpleanios());
        double sumaVentas =0;
        for (int i =0 ;i<ventas.size();i++){
            if (Month.from(ventas.get(i).getFecha()).equals(mes)){
                sumaVentas += ventas.get(i).getMonto();
            }
        }
        return monto + (sumaVentas * (0.05) + 2000);
    }

    public void vender(double monto){
        Venta nuevaVenta = new Venta(LocalDate.now(),monto);
        ventas.add(nuevaVenta);
    }
    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public float getPorcentaje() {
        return porcentaje;
    }
}
