package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;

public class PorHora extends Empleado {
    private float valorPorHora;
    private ArrayList<Hora> horas = new ArrayList<>();

    public PorHora(String nombre, String apellido, String telefono,
                   String cuit, LocalDate fechaDeCumpleanios, float valorPorHora) {
        super(nombre, apellido, telefono, cuit, fechaDeCumpleanios);
        this.valorPorHora = valorPorHora;
    }

    @Override
    public double calculoPorCumpleanios(double monto) {
        return monto + 2000;
    }

    @Override
    public double calcularSueldoClase(Month mes) {
        double sumaHoras=0;
        for (int i=0;i<horas.size();i++){
            if (Month.from(horas.get(i).getFecha()).equals(mes)){
                sumaHoras += horas.get(i).getHoras();
            }
        }
        if (sumaHoras > 40){
            return (sumaHoras*getValorPorHora())*1.20;
        }else{
            return sumaHoras*getValorPorHora();
        }

    }
    public void sumarHorasTrabajadas(LocalDate fecha, int horas){
        Hora nuevaHora = new Hora(LocalDate.now(),horas);
        this.horas.add(nuevaHora);
    }

    public float getValorPorHora() {
        return valorPorHora;
    }
}
