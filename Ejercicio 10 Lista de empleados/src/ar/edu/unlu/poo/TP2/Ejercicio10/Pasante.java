package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;
import java.time.Month;

public class Pasante extends Empleado {
    public Pasante(String nombre, String apellido, String telefono,
                   String cuit, LocalDate fechaDeCumpleanios) {
        super(nombre, apellido, telefono, cuit, fechaDeCumpleanios);
    }

    @Override
    public double calcularSueldoClase(Month mes) {
        return 0;
    }

    @Override
    public double calculoPorCumpleanios(double monto) {
        return 0;
    }
}
