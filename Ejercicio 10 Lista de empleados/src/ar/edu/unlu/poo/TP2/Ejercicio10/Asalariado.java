package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;
import java.time.Month;

public class Asalariado extends Empleado {
    private double sueldo;

    public Asalariado(String nombre, String apellido, String telefono,
                      String cuit, LocalDate fechaDeCumpleanios, double sueldo) {
        super(nombre, apellido, telefono, cuit, fechaDeCumpleanios);
        this.sueldo = sueldo;
    }

    @Override
    public double calculoPorCumpleanios(double monto) {
        return monto+3000;
    }

    public double calcularSueldoClase(Month mes){
        return this.sueldo;
    }
}
