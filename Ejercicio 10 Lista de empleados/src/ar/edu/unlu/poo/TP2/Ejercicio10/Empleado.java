package ar.edu.unlu.poo.TP2.Ejercicio10;

import java.time.LocalDate;
import java.time.Month;

public abstract class Empleado {
    private String nombre;
    private String apellido;
    private String telefono;
    private String cuit;
    private LocalDate fechaDeCumpleanios;

    public Empleado(String nombre, String apellido, String telefono,
                    String cuit, LocalDate fechaDeCumpleanios) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.cuit = cuit;
        this.fechaDeCumpleanios = fechaDeCumpleanios;
    }
    public abstract double calcularSueldoClase(Month mes);
    public abstract double calculoPorCumpleanios(double monto);
    public double calcularSueldo(Month mes){

            if (esSuCumpleanios(mes)){
                return this.calculoPorCumpleanios(this.calcularSueldoClase(mes));
            }else{
                return this.calcularSueldoClase(mes);
            }
    }
    public boolean esSuCumpleanios(Month mes){
        if (Month.from(fechaDeCumpleanios).equals(mes)){
            return true;
        }else{
            return false;
        }
    }
    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCuit() {
        return cuit;
    }

    public LocalDate getFechaDeCumpleanios() {
        return fechaDeCumpleanios;
    }
}
