# Trabajo Practico Nro 2



## Diego Emanuel Burrieza


## Legajo: 182.876

## En la clase CuentaNormal, el metodo gastar, hice una modificacion:
## Cambie lo que estaba devolviendo, en ves de un boleano hice una clase nueva que contiene un mensaje y un boleano, para enviar el mensaje que el gasto pudo ser hecho o no y usando o no el giro de descubierto.
## La otra opcion que habia era verificarlo antes de enviar el mensaje de gasto a la cuenta normal, comparando el monto a gastar con el saldo que tiene y ahi avisar antes de ejecutar el gasto.
## Son diferentes formas de implementar. yo usaria la segunda opcion en caso que pueda codigicar la clase que envia el mensaje a cuenta Normal.


