package ar.edu.unlu.poo.tp2.ejercicio4y5;
import java.time.LocalDate;
public class CuentaNormal {
        private double saldo;
        private double limiteGiroDescubierto;
        private double giroDescubierto;
        private double saldoInvertido;
        public static final double INTERES_POR_INVERSION = 0.05;
        public static final double PLAZO_DIAS_INVERSION = 30;
        private LocalDate fechaInversion;
        private boolean precancelacion;

        public CuentaNormal(double saldo, double limiteGiroDescubierto) {
            this.saldo = saldo;
            this.limiteGiroDescubierto = limiteGiroDescubierto;
            this.giroDescubierto = 0;
            this.saldoInvertido = 0;
            this.fechaInversion = null;
        }
        public boolean getPrecancelacion(){
            return this.precancelacion;
        }
        public void setPrecancelacion(boolean accion){
            this.precancelacion = accion;
        }

        /**
         * Dado un monto genera un gasto en la cuenta: quita del saldo el monto a gastar, si el monto es mayor
         * al saldo entonces gira en descubierto (siempre y cuando todavía quede límite).
         * Si (saldo + limite descubierto disponible) < monto, entonces la operacion no se realiza y devuelve false.
         * @param monto: el monto a gastar
         * @return boolean: indica si la operación fué exitosa.
         */

        public RespuestaGasto gastar(double monto) {
            RespuestaGasto res = new RespuestaGasto("El gasto no pudo efectuarce",false);
            if (monto > getSaldo() && this.precancelacion && getMontoInvertido() >0){
                if (fechaInversion.plusDays((long)this.PLAZO_DIAS_INVERSION).isBefore(LocalDate.now())&&
                        monto <= getSaldo() + getLimiteGiroDescubierto()+(getMontoInvertido()*this.INTERES_POR_INVERSION) ){
                    monto -= this.saldo;
                    this.saldo = 0;
                    if (monto > this.saldoInvertido+(this.saldoInvertido*this.INTERES_POR_INVERSION) ){
                        monto -= this.saldoInvertido+(this.saldoInvertido*this.INTERES_POR_INVERSION);
                        this.saldoInvertido = 0;
                        this.giroDescubierto -= monto;
                        res = new RespuestaGasto("El gasto fue hecho usando el saldo invertido y el giro descubierto",true);
                        return res;
                    }else{
                        this.saldoInvertido -= monto;
                        res = new RespuestaGasto("El gasto fue hecho usando el saldo invertido",true);
                        return res;
                    }
                }
            }else if ((getSaldo() + getLimiteGiroDescubierto()) >= monto) {
                if (getSaldo() < monto) {
                    //Giro en descubierto
                    this.giroDescubierto += monto - getSaldo();
                    this.saldo = 0;
                    res = new RespuestaGasto("El gasto fue hecho usando el giro descubierto",true);
                    return res;
                }else {
                    // El saldo me alzanza para el gasto
                    this.saldo -= monto;
                    res = new RespuestaGasto("El gasto fue hecho sin usar el giro descubierto",true);
                    return res;
                }
            }else {
                res = new RespuestaGasto("No se pùdo hacer la transaccion!",false);
                return res;
            }
            return res;
        }

        /**
         * Deposita el monto en la cuenta. Si existe giro en descubierto, primero intenta cubrirlo y si queda
         * dinero disponible aumenta el saldo.
         * @param monto
         */
        public void depositar(double monto) {
            if(getGiroDescubierto() ==0){
                this.saldo += monto;
            }else if (getGiroDescubierto()<monto){
                monto -= getGiroDescubierto();
                this.giroDescubierto = 0;
                this.saldo += monto;
            } else if (getGiroDescubierto() == monto) {
                this.giroDescubierto = 0;
            } else if (getGiroDescubierto()>monto) {
                this.giroDescubierto -= monto;
            }
        }

        /**
         * Realiza la inversion del monto indicado. Condiciones para que la operación sea exitosa:
         * 	a. Que el saldo sea >= monto
         *  b. Que no exista una inversión activa.
         *
         * Tambien establece la fecha de inversión.
         *
         * @param monto
         * @return
         */
        public boolean invertir(double monto) {
            if (getSaldo() >= monto && getMontoInvertido() == 0){
                this.saldoInvertido = monto;
                this.saldo -= monto;
                this.fechaInversion = LocalDate.now();
                return true;
            } else {
                return false;
            }
        }

        /**
         * Devuelve el monto invertido al saldo con el interes establecido. Se puede realizar siempre y cuando
         * hayan pasado los N días que dura la inversión.
         * @return
         */
        public boolean recuperarInversion() {
            if(fechaInversion.plusDays(30).isBefore(LocalDate.now()) || fechaInversion.plusDays(30).isEqual(LocalDate.now())){
                this.fechaInversion = null;
                this.saldo += (getInteresAGanar()+getMontoInvertido());
                this.saldoInvertido = 0;
                return true;
            }else{
                return false;
            }
        }

        public double getSaldo() {
            return this.saldo;
        }

        public double getGiroDescubierto() {
            return this.giroDescubierto;
        }

        public double getLimiteGiroDescubierto() {
            return this.limiteGiroDescubierto;
        }

        public double getMontoInvertido() {
            return this.saldoInvertido;
        }

        public double getInteresAGanar() {
            if (this.fechaInversion != null)
                return this.saldoInvertido * CuentaNormal.INTERES_POR_INVERSION;
            return 0.0d;
        }
    }
