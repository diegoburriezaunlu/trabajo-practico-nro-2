package ar.edu.unlu.poo.tp2.ejercicio14;

public class GuiaTuristico extends Proveedores{

    public GuiaTuristico(String descripcion, Double valor, String empresa,
                         String destino, Double costoBase) {
        super(descripcion, valor, empresa, destino, costoBase);
    }

    @Override
    public double calcularExtra(Paquete paquete) {
        if(Paquete.getPasajerosVendidos()>200){
            return this.getCostoBase()+((double) 90 /Paquete.getPasajerosVendidos());
        }else{
            return this.getCostoBase();
        }
    }
}
