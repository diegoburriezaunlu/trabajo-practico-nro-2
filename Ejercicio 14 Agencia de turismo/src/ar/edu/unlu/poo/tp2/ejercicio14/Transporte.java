package ar.edu.unlu.poo.tp2.ejercicio14;

public class Transporte extends Proveedores{

    public Transporte(String descripcion, Double valor, String empresa,
                      String destino, Double costoBase) {
        super(descripcion, valor, empresa, destino, costoBase);
    }

    @Override
    public double calcularExtra(Paquete paquete) {
        return this.getCostoBase()*1.03;
    }
}
