package ar.edu.unlu.poo.tp2.ejercicio14;

public abstract class Proveedores {
    private String descripcion;
    private Double valor;
    private String empresa;
    private String destino;
    private Double costoBase;


    public Proveedores(String descripcion, Double valor, String empresa,
                       String destino, Double costoBase) {
        this.descripcion = descripcion;
        this.valor = valor;
        this.empresa = empresa;
        this.destino = destino;
        this.costoBase = costoBase;

    }
    public abstract double calcularExtra(Paquete paquete);

    public String getDescripcion() {
        return descripcion;
    }

    public Double getValor() {
        return valor;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getDestino() {
        return destino;
    }

    public Double getCostoBase() {
        return costoBase;
    }


}
