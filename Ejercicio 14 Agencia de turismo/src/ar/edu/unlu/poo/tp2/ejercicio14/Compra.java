package ar.edu.unlu.poo.tp2.ejercicio14;

import java.time.LocalDate;

public class Compra {
    private Cliente clliente;
    private Paquete paquete;
    private LocalDate fechaDeCompra;

    public Compra(Cliente clliente, Paquete paquete) {
        this.clliente = clliente;
        this.paquete = paquete;
        this.fechaDeCompra = LocalDate.now();
    }

    public Cliente getClliente() {
        return clliente;
    }

    public Paquete getPaquete() {
        return paquete;
    }

    public LocalDate getFechaDeCompra() {
        return fechaDeCompra;
    }
}
