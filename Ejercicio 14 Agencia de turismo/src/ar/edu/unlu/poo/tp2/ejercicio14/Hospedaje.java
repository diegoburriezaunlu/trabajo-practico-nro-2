package ar.edu.unlu.poo.tp2.ejercicio14;

public class Hospedaje extends Proveedores{
    private int clientesMinima;

    public Hospedaje(String descripcion, Double valor, String empresa, String destino, Double costoBase, int clientesMinima) {
        super(descripcion, valor, empresa, destino, costoBase);
        this.clientesMinima = clientesMinima;
    }


    public double calcularExtra(Paquete paquete) {
        if (paquete.getCantDeClientesMaxima()>this.clientesMinima){
            return this.getCostoBase() - (this.getCostoBase()*0.05);
        }else{
            return this.getCostoBase();
        }
    }
}
