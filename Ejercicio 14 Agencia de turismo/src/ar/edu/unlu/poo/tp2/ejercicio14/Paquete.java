package ar.edu.unlu.poo.tp2.ejercicio14;

import java.util.ArrayList;

public class Paquete {
    private static int pasajerosVendidos;
    private ArrayList<Proveedores> servicios = new ArrayList<>();
    private int cantDeClientesMaxima;
    private String destino;

    public Paquete(Transporte servicioTransporte,GuiaTuristico servicioGuiaTurismo,
                   Hospedaje servicioHospedaje, int cantDeClientesMaxima, String destino) {
        servicios.add(servicioTransporte);
        servicios.add(servicioGuiaTurismo);
        servicios.add(servicioHospedaje);
        this.cantDeClientesMaxima = cantDeClientesMaxima;
        this.destino = destino;
    }
    public GuiaTuristico consultarGuiaTurismo(){
        GuiaTuristico res=null;
        for (int i =0;i<servicios.size();i++) {
            if (servicios.get(i) instanceof GuiaTuristico){
                res = (GuiaTuristico) servicios.get(i);
            }
        }
        return res;
    }
    public Transporte consultarTransporte(){
        Transporte res=null;
        for (int i =0;i<servicios.size();i++) {
            if (servicios.get(i) instanceof Transporte){
                res = (Transporte) servicios.get(i);
            }
        }
        return res;
    }
    public Hospedaje consultarHospedaje(){
        Hospedaje res=null;
        for (int i =0;i<servicios.size();i++) {
            if (servicios.get(i) instanceof Hospedaje){
                res = (Hospedaje) servicios.get(i);
            }
        }
        return res;
    }

    public static void setPasajerosVendidos(int pasajerosVendidos) {
        Paquete.pasajerosVendidos = pasajerosVendidos;
    }

    public static int getPasajerosVendidos() {
        return pasajerosVendidos;
    }

    public int getCantDeClientesMaxima() {
        return cantDeClientesMaxima;
    }

    public String getDestino() {
        return destino;
    }
}
