package ar.edu.unlu.poo.tp2.ejercicio9;

public class Tetraedro extends Figura3D {
    private double arista;

    public Tetraedro(double arista) {
        this.arista = arista;
    }

    public double getArista() {
        return arista;
    }

    public void setArista(double arista) {
        this.arista = arista;
    }
    @Override
    public double calcularArea(){
        return Math.pow(arista,2)*Math.sqrt(3.00);
    }
    @Override
    public double calcularVolumen(){
        return Math.pow(arista,3)*(Math.sqrt(2)/12);
    }
}
