package ar.edu.unlu.poo.tp2.ejercicio9;

public class Circulo extends Figura2D{
    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    @Override
    public double calcularArea(){
         return Math.PI*Math.pow(this.radio,2);
    }
}
