package ar.edu.unlu.poo.tp2.ejercicio9;

public abstract class Figura3D extends Figura{

    public abstract double calcularArea();
    public abstract double calcularVolumen();
}
