package ar.edu.unlu.poo.tp2.ejercicio9;

public class Cubo extends Figura3D{
    private double arista;

    public Cubo(double arista) {
        this.arista = arista;
    }

    public double getArista() {
        return arista;
    }

    public void setArista(double arista) {
        this.arista = arista;
    }
    @Override
    public double calcularArea(){
        return 6.0*Math.pow(arista,2);
    }
    @Override
    public double calcularVolumen(){
        return Math.pow(arista,3);
    }
}
