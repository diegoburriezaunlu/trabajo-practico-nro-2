package ar.edu.unlu.poo.tp2.ejercicio9;

public class Cuadrado extends Figura2D{
    private double lado;


    public Cuadrado(double lado1) {
        this.lado = lado1;

    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
    @Override
    public double calcularArea(){
        return lado *lado;
    }
}
