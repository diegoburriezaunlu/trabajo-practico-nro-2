package ar.edu.unlu.poo.tp2.ejercicio9;

public abstract class Figura {

    public abstract double calcularArea();
}
