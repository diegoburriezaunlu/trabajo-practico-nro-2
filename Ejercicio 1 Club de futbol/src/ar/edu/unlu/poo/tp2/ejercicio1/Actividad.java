package ar.edu.unlu.poo.tp2.ejercicio1;

public class Actividad {
    private String descripcion;
    private Suscripcion suscripcion;


    public Actividad(String descripcion, Suscripcion suscripcion){
        this.descripcion = descripcion;
        this.suscripcion = suscripcion;
    }
    String getDescripcion(){
        return this.descripcion;
    }
    Suscripcion getSuscripcion(){
        return this.suscripcion;
    }
}
