package ar.edu.unlu.poo.tp2.ejercicio1;

import java.time.LocalDate;
import java.time.Month;

public class Socio {
    private String nombre;
    private String apellido;
    private String dni;
    private String tel;
    private String domicilio;
    private String email;
    private Suscripcion suscripcion;
    private LocalDate fechaInscripcion;

    String getNombre(){
        return this.nombre;
    }
    String getApellido(){
        return this.apellido;
    }
    Month consultarMesInscripcion(){
        return fechaInscripcion.getMonth();
    }
    String registrarSocio(String nombre, String apellido, String dni, String tel,
                 String domicilio, String email, Suscripcion suscripcion){
        this.nombre=nombre;
        this.apellido=apellido;
        this.dni=dni;
        this.tel=tel;
        this.domicilio=domicilio;
        this.email=email;
        this.suscripcion=suscripcion;
        this.fechaInscripcion = LocalDate.now();

        return generarCredencial();
    }
    private String generarCredencial(){
        return"****************\n*SOCIO DEL CLUB*\n" + nombre +" "+apellido + "\n"+"* SUSCRIPCION  * \n"
                +"##"+suscripcion+"##\n*****************";
    }

}
