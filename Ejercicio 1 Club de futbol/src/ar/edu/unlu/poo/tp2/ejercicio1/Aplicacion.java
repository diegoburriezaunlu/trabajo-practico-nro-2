package ar.edu.unlu.poo.tp2.ejercicio1;


import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Scanner;

public class Aplicacion {
    private ArrayList<Socio> socios = new ArrayList<>();
    private ArrayList<Actividad> actividades = new ArrayList<>();

    public void mostarMenu(){
        //Cargo las actividades en forma automatica
        Actividad nuevaActividad = new Actividad("Baños",Suscripcion.BASICA);
        actividades.add(nuevaActividad);
        nuevaActividad = new Actividad("Salon",Suscripcion.BASICA);
        actividades.add(nuevaActividad);
        nuevaActividad = new Actividad("Canchas",Suscripcion.INTERMEDIA);
        actividades.add(nuevaActividad);
        nuevaActividad = new Actividad("Mesas",Suscripcion.INTERMEDIA);
        actividades.add(nuevaActividad);
        nuevaActividad = new Actividad("Pileta",Suscripcion.DESTACADA);
        actividades.add(nuevaActividad);
        nuevaActividad = new Actividad("Bicicletas",Suscripcion.DESTACADA);
        actividades.add(nuevaActividad);
        Scanner sc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("****************************************************");
            System.out.println("          1. Registrar Socio");
            System.out.println("          2. Reporte de inscripcion mensual");
            System.out.println("          3. Listado de actividades por suscripcion");
            System.out.println("          0. Salir");
            System.out.println("****************************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    System.out.println("INGRESE NOMBRE: ");
                    sc.nextLine();
                    String nombre = sc.nextLine();
                    System.out.println("INGRESE APELLIDO: ");
                    String apellido = sc.nextLine();
                    System.out.println("INGRESE DNI: ");
                    String dni = sc.nextLine();
                    System.out.println("INGRESE TELEFONO: ");
                    String tel = sc.nextLine();
                    System.out.println("INGRESE DOMICILIO: ");
                    String domicilio = sc.nextLine();
                    System.out.println("INGRESE EMAIL: ");
                    String email = sc.nextLine();
                    System.out.println("INGRESE SUSCRIPCION:  <<BASICA,INTERMEDIA,DESTACADA>>");
                    Suscripcion suscripcion = Suscripcion.valueOf(sc.nextLine());
                    sc.nextLine();
                    System.out.println(registrarSocio(nombre,apellido,dni,tel,domicilio,email,suscripcion));
                    sc.nextLine();
                    break;
                case 2:
                    sc.nextLine();
                    System.out.println("INGRESE EL MES DEL REPORTE EN NUMERO DEL 1 AL 12: ");
                    Month mes = Month.of(Integer.parseInt(sc.nextLine()));
                    System.out.println(reporteInscripcionMensual(mes));
                    sc.nextLine();
                    break;
                case 3:
                    System.out.println(listadoPorSuscripcion());
                    sc.nextLine();
                    sc.nextLine();
                    break;
                default:
                    System.out.println("Solo numeros entre 0 y 2");

            }
        }
    }
    private String registrarSocio(String nombre, String apellido, String dni,
                                  String tel, String domicilio, String email,
                                  Suscripcion suscripcion){
        Socio nuevoSocio = new Socio();
        String credencial = nuevoSocio.registrarSocio(nombre,apellido,
                dni,tel,domicilio,email,suscripcion);
                socios.add(nuevoSocio);
        return credencial;
    }
    private String reporteInscripcionMensual(Month mes){
        String respuesta= "SOCIOS REGISTRADOS EN "+mes+": \n";
        for (int i=0;i<socios.size();i++){
            if (socios.get(i).consultarMesInscripcion().equals(mes)){
                respuesta += socios.get(i).getApellido() + " " + socios.get(i).getNombre()+"\n";
            }
        }
        return respuesta;
    }
    private String listadoPorSuscripcion(){
        String respuesta = "Actividades con suscripcion <<BASICA>>: \n";
        for (int i=0; i<actividades.size();i++){
            if (actividades.get(i).getSuscripcion().equals(Suscripcion.BASICA)){
                respuesta += "# "+actividades.get(i).getDescripcion() + "\n";
            }
        }
        respuesta += "\nActividades con suscripcion <<INTERMEDIA>>: \n";
        for (int i=0; i<actividades.size();i++){
            if (actividades.get(i).getSuscripcion().equals(Suscripcion.INTERMEDIA)||
                    actividades.get(i).getSuscripcion().equals(Suscripcion.BASICA)){
                respuesta += "# "+actividades.get(i).getDescripcion() + "\n";
            }
        }
        respuesta += "\nActividades con suscripcion <<DESTACADA>>: \n";
        for (int i=0; i<actividades.size();i++){
            if (actividades.get(i).getSuscripcion().equals(Suscripcion.DESTACADA)||
                    actividades.get(i).getSuscripcion().equals(Suscripcion.INTERMEDIA)||
                    actividades.get(i).getSuscripcion().equals(Suscripcion.BASICA)){
                respuesta += "# "+actividades.get(i).getDescripcion() + "\n";
            }
        }
        return respuesta;
    }
}
