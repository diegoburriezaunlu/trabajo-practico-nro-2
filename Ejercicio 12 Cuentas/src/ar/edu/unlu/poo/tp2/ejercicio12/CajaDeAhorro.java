package ar.edu.unlu.poo.tp2.ejercicio12;

import java.time.LocalDate;

public class CajaDeAhorro  implements Cuenta{
    private double saldo;
    private double saldoInvertido;
    public static final double INTERES_POR_INVERSION = 0.05;
    public static final double PLAZO_DIAS_INVERSION = 30;
    private LocalDate fechaInversion;
    private boolean precancelacion;

    public CajaDeAhorro(double saldo) {
        this.saldo = saldo;
        this.saldoInvertido =0;
        this.fechaInversion = null;
    }

    public void setSaldoInvertido(double saldoInvertido) {
        this.saldoInvertido = saldoInvertido;
    }

    public boolean getPrecancelacion(){
        return this.precancelacion;
    }
    public void setPrecancelacion(boolean accion){
        this.precancelacion = accion;
    }
    public boolean invertir(double monto) {
        if (getSaldo() >= monto && getSaldoInvertido() == 0){
            this.saldoInvertido = monto;
            this.saldo -= monto;
            this.fechaInversion = LocalDate.now();
            return true;
        } else {
            return false;
        }
    }
    public boolean recuperarInversion() {
        if(fechaInversion.plusDays(30).isBefore(LocalDate.now()) || fechaInversion.plusDays(30).isEqual(LocalDate.now())){
            this.fechaInversion = null;
            this.saldo += (getInteresAGanar()+getSaldoInvertido());
            this.saldoInvertido = 0;
            return true;
        }else{
            return false;
        }
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getSaldo() {
        return this.saldo;
    }
    public double getSaldoInvertido() {
        return this.saldoInvertido;
    }

    public double getInteresAGanar() {
        if (this.fechaInversion != null)
            return this.saldoInvertido * CuentaNormal.INTERES_POR_INVERSION;
        return 0.0d;
    }

    public LocalDate getFechaInversion() {
        return fechaInversion;
    }

    public RespuestaGasto gastar(double monto) {
        RespuestaGasto res = new RespuestaGasto("El gasto no pudo efectuarce",false);
        if(monto<= (saldo+saldoInvertido)){
            if (monto<=saldo) {
                saldo -= monto;
                res = new RespuestaGasto("El gasto pudo efectuarce",true);
            }else if (fechaInversion.plusDays((long)this.PLAZO_DIAS_INVERSION).isBefore(LocalDate.now())){
                monto -= this.saldo;
                this.saldo = 0;
                this.saldoInvertido -= monto;
                res = new RespuestaGasto("El gasto fue hecho usando el saldo invertido",true);
                return res;
                }
            }
        return res;
    }
    public void depositar(double monto) {
        this.saldo += monto;
    }
}
