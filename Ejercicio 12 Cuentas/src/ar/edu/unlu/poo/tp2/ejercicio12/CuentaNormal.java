package ar.edu.unlu.poo.tp2.ejercicio12;
import java.time.LocalDate;
public class CuentaNormal extends CajaDeAhorro{
        private double limiteGiroDescubierto;
        private double giroDescubierto;


        public CuentaNormal(double saldo, double limiteGiroDescubierto) {
            super(saldo);
            this.limiteGiroDescubierto = limiteGiroDescubierto;
            this.giroDescubierto = 0;

        }

        public RespuestaGasto gastar(double monto) {
            RespuestaGasto res = new RespuestaGasto("El gasto no pudo efectuarce",false);
            if (monto > getSaldo() && this.getPrecancelacion() && getSaldoInvertido() >0){
                if (getFechaInversion().plusDays((long)this.PLAZO_DIAS_INVERSION).isBefore(LocalDate.now())&&
                        monto <= getSaldo() + getLimiteGiroDescubierto()+(getSaldoInvertido()*this.INTERES_POR_INVERSION) ){
                    monto -= this.getSaldo();
                    this.setSaldo(0);
                    if (monto > this.getSaldoInvertido()+(this.getSaldoInvertido()*this.INTERES_POR_INVERSION) ){
                        monto -= this.getSaldoInvertido()+(this.getSaldoInvertido()*this.INTERES_POR_INVERSION);
                        this.setSaldoInvertido(0);
                        this.giroDescubierto -= monto;
                        res = new RespuestaGasto("El gasto fue hecho usando el saldo invertido y el giro descubierto",true);
                        return res;
                    }else{
                        this.setSaldoInvertido(this.getSaldoInvertido()-monto);
                        res = new RespuestaGasto("El gasto fue hecho usando el saldo invertido",true);
                        return res;
                    }
                }
            }else if ((getSaldo() + getLimiteGiroDescubierto()) >= monto) {
                if (getSaldo() < monto) {
                    //Giro en descubierto
                    this.giroDescubierto += monto - getSaldo();
                    this.setSaldo(0);
                    res = new RespuestaGasto("El gasto fue hecho usando el giro descubierto",true);
                    return res;
                }else {
                    // El saldo me alzanza para el gasto
                    this.setSaldo(getSaldo()-monto);
                    res = new RespuestaGasto("El gasto fue hecho sin usar el giro descubierto",true);
                    return res;
                }
            }else {
                res = new RespuestaGasto("No se pùdo hacer la transaccion!",false);
                return res;
            }
            return res;
        }

        public void depositar(double monto) {
            if(getGiroDescubierto() ==0){
                this.setSaldo(getSaldo()+monto);
            }else if (getGiroDescubierto()<monto){
                monto -= getGiroDescubierto();
                this.giroDescubierto = 0;
                this.setSaldo(getSaldo()+monto);
            } else if (getGiroDescubierto() == monto) {
                this.giroDescubierto = 0;
            } else if (getGiroDescubierto()>monto) {
                this.giroDescubierto -= monto;
            }
        }

        public double getGiroDescubierto() {
            return this.giroDescubierto;
        }

        public double getLimiteGiroDescubierto() {
            return this.limiteGiroDescubierto;
        }


    }
