package ar.edu.unlu.poo.tp2.ejercicio12;

public class CompraACredito {

    private double monto;
    private double interes;

    public CompraACredito(double monto){
        this.monto = monto;
        this.interes = calculoInteres(monto);
    }
    private double calculoInteres(double monto){
        return (monto*0.05);
    }
    boolean pagar(double monto){
        if (this.monto >= monto){
            this.monto-= monto;
            return true;
        } else if ((this.monto + this.interes)>=monto){
                monto -= this.monto;
                this.monto = 0;
                this.interes -= monto;
                return true;
        }else{
            return false;
        }
    }
    double getMonto(){
        return this.monto;
    }
    double getInteres(){
        return this.getInteres();
    }
    double getSaldoDeudor(){
        return this.monto + this.interes;
    }
}
