package ar.edu.unlu.poo.tp2.ejercicio12;

public class RespuestaGasto {
    private String mensaje;
    private boolean hecho;

    public RespuestaGasto(String mensaje, boolean hecho){
        this.mensaje = mensaje;
        this.hecho = hecho;
    }
    public String getMensaje(){
        return this.mensaje;
    }
    public boolean getHecho(){
        return this.hecho;
    }
}
