package ar.edu.unlu.poo.tp2.ejercicio12;
import java.util.ArrayList;

public class CuentaCredito implements Cuenta{
        private double limiteCredito;
        private ArrayList<CompraACredito> compras;


        public CuentaCredito(double limite) {
            this.limiteCredito = limite;
            this.compras = new ArrayList<CompraACredito>();
        }

        /**
         * Registra una nueva compra en caso que todavía quede saldo disponible para compras.
         *
         * @param monto
         * @return boolean: true si la compra fue existosa, false en caso contrario.
         */
        public boolean comprar(double monto) {
            if (this.limiteCredito>=monto) {
                this.limiteCredito -= monto;
                CompraACredito nuevaCompra = new CompraACredito(monto);
                this.compras.add(nuevaCompra);
                return true;
            } else{
                return false;
            }
        }

        /**
         * Realiza un pago sobre una compra. Si el monto es menor o igual al saldo que queda por pagar de la compra, el pago se registra
         * y la operacion es exitosa devolviendo true.
         * Si el monto es mayor al saldo, entonces la operación no se realiza devolviendo false.
         *
         * @param monto: cantidad a pagar.
         * @param indiceCompra: el numero de indice de la compra sobre la cual se requiere realizar un pago.
         * @return
         */
        public boolean pagar(double monto, int indiceCompra) {
            if(indiceCompra<compras.size()){
                double deuda = compras.get(indiceCompra).getMonto();
                double nuevaDeuda =0;
               if (compras.get(indiceCompra).pagar(monto)){
                    this.limiteCredito += (deuda-(compras.get(indiceCompra).getMonto()));
                    return true;
                    //cuando la deuda quede en 0 podriamos remover el objeto del array, pero
                   // esto causaria un movimiento en los index. (Tendria que tenerse en cuenta ese tema).
               }else{
                   return false;
               }
            }else{
                return false;
            }
        }

        /**
         * Retorna el saldo que queda por pagar de una compra, incluyendo el interes.
         *
         * @param indiceCompra
         * @return
         */
        public double getSaldoDeudorCompra(int indiceCompra) {
            return compras.get(indiceCompra).getSaldoDeudor();
        }

        /**
         * Devuelve el saldo deudor total teniendo en cuenta todas las compras impagas.
         * @return double
         */
        public double getSaldoDeudor() {
            double respuesta = 0;
            for (int i =0; i<compras.size();i++){
                respuesta += compras.get(i).getSaldoDeudor();
            }
            return respuesta;
        }

        /**
         * Devuelve el monto disponible para compras de la cuenta teniendo en cuenta todas las compras
         * realizadas que quedan por pagar, sin tener en cuenta las que deben solo el interes.
         * @return double: el saldo disponible para realizar compras.
         */
        public double getMontoDisponibleParaCompras() {
            return this.limiteCredito;
        }

    }
