package ar.edu.unlu.poo.tp2.Ejercicio13;

import java.time.LocalDate;

public class Ticket {
    private Double valor;
    private LocalDate fecha;
    private String asiento;

    public Ticket(Double valor, LocalDate fecha, String asiento) {
        this.valor = valor;
        this.fecha = fecha;
        this.asiento = asiento;
    }

    public Double getValor() {
        return valor;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public String getAsiento() {
        return asiento;
    }
}
