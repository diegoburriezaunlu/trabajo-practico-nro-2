package ar.edu.unlu.poo.tp2.Ejercicio13;

import java.time.LocalDate;

public class Pasajero implements Rol{
    private Ticket ticket;

    public Pasajero(Double valor, LocalDate fecha, String asiento) {
        this.ticket = new Ticket(valor,fecha,asiento);
    }

    public Ticket getTicket() {
        return ticket;
    }
}
