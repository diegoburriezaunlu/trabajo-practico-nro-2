package ar.edu.unlu.poo.tp2.Ejercicio13;

import java.time.LocalDate;

public class Tripulante implements Rol{

    private String Cargo;
    private ReciboSueldo reciboSueldo;

    public Tripulante(String cargo, Double sueldo, LocalDate fecha) {
        Cargo = cargo;
        this.reciboSueldo = new ReciboSueldo(sueldo,fecha);

    }

    public String getCargo() {
        return Cargo;
    }

    public ReciboSueldo getReciboSueldo() {
        return reciboSueldo;
    }
}
