package ar.edu.unlu.poo.tp2.Ejercicio13;

import java.time.LocalDate;
import java.util.ArrayList;

public class Persona {
    private String nombre;
    private String apellido;
    private String dni;
    private ArrayList<Rol> roles = new ArrayList<>();

    public Persona(String nombre, String apellido, String dni, String cargo,
                   Double sueldo, LocalDate fecha) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        Tripulante tripulante = new Tripulante(cargo,sueldo,fecha);
        roles.add(tripulante);
    }
    public Persona(String nombre, String apellido, String dni,LocalDate fecha,
                   Double valorPasaje, String asiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        Pasajero pasajero = new Pasajero(valorPasaje,fecha,asiento);
        roles.add(pasajero);
    }
    public Persona(String nombre, String apellido, String dni, String cargo,
                   Double sueldo, LocalDate fecha,Double valorPasaje, String asiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        Tripulante tripulante = new Tripulante(cargo,sueldo,fecha);
        roles.add(tripulante);
        Pasajero pasajero = new Pasajero(valorPasaje,fecha,asiento);
        roles.add(pasajero);
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public ArrayList<Rol> getRoles() {
        return roles;
    }
}
