package ar.edu.unlu.poo.tp2.Ejercicio13;

import java.time.LocalDate;

public class ReciboSueldo {
    private Double sueldo;
    private LocalDate fecha;

    public ReciboSueldo(Double sueldo, LocalDate fecha) {
        this.sueldo = sueldo;
        this.fecha = fecha;
    }

    public Double getSueldo() {
        return sueldo;
    }

    public LocalDate getFecha() {
        return fecha;
    }
}
