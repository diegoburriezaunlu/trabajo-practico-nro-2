package ar.edu.unlu.poo.tp2.Ejercicio13;

import java.time.LocalDate;
import java.util.ArrayList;

public class Vuelo {
    private String descripcion;
    private String destino;
    private LocalDate fecha;
    private String avion;
    private String empresa;
    ArrayList<Persona> personas = new ArrayList<>();

    public Vuelo(String descripcion, String destino, LocalDate fecha, String avion, String empresa) {
        this.descripcion = descripcion;
        this.destino = destino;
        this.fecha = fecha;
        this.avion = avion;
        this.empresa = empresa;
    }
    public void agregarPersona(String nombre, String apellido, String dni, String cargo,
                               Double sueldo){
        Persona persona = new Persona(nombre,apellido,dni,cargo,sueldo,this.fecha);
        this.personas.add(persona);

    }
    public void agregarPersona(String nombre, String apellido, String dni,
                               Double valorPasaje, String asiento){
        Persona persona = new Persona(nombre,apellido,dni,this.fecha,valorPasaje,asiento);
        this.personas.add(persona);
    }
    public void agregarPersona(String nombre, String apellido, String dni, String cargo,
                               Double sueldo, LocalDate fecha,Double valorPasaje, String asiento){
        Persona persona = new Persona(nombre,apellido,dni,cargo,sueldo,this.fecha,valorPasaje,asiento);
        this.personas.add(persona);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDestino() {
        return destino;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public String getAvion() {
        return avion;
    }

    public String getEmpresa() {
        return empresa;
    }

    public ArrayList<Persona> getPersonas() {
        return personas;
    }
}
