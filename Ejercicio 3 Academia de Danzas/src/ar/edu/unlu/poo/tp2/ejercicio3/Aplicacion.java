package ar.edu.unlu.poo.tp2.ejercicio3;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.*;

public class Aplicacion {

    private ArrayList<Diagramacion> diagramaciones = new ArrayList<>();
    Scanner sc = new Scanner(System.in);
    public void mostrarMenu(){
        //cargo diagramaciones en forma aleatoria.
        for (int i=0; i<300;i++){
            Diagramacion nuevaDiagramacion = new  Diagramacion(Disciplina.values()[(int) (Math.random()*Disciplina.values().length)]
                    ,Profesor.values()[(int) (Math.random()*(Profesor.values().length))],
                    Salon.values()[(int) (Math.random()*(Salon.values().length))],
                    DayOfWeek.values()[(int) (Math.random()*(DayOfWeek.values().length))],
                    LocalTime.of((int) ((Math.random()*12)+8),00));
            diagramaciones.add(nuevaDiagramacion);
        }

        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("****************************************************");
            System.out.println("          1. Inscribir alumno");
            System.out.println("          2. Listado de alumnos por profesor");
            System.out.println("          3. Disciplina mas elejida");
            System.out.println("          0. Salir");
            System.out.println("****************************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    System.out.println(inscribirAlumno());
                    sc.nextLine();
                    break;
                case 2:
                    System.out.println("******************************************************");
                    for (int i=0;i<Profesor.values().length;i++){
                        System.out.println(Profesor.values()[i]);
                    }

                    System.out.println("******************************************************");
                    sc.nextLine();
                    System.out.println("Elija el profesor a liquidar sus honorarios");
                    Profesor profesor = Profesor.valueOf(sc.nextLine());
                    System.out.println("Ingrese el mes en numero del 1-12: ");
                    Month mes = Month.of(sc.nextInt());
                    System.out.println(listoAlumnosPorDiagramacion(profesor,mes));


                    break;
                case 3:
                    System.out.println("Ingrese el mes en numero del 1-12: ");
                    Month mes1 = Month.of(sc.nextInt());
                    System.out.println(disciplinaMasElejida(mes1));
                    break;
                default:
                    System.out.println("Solo numeros entre 1 y 10");

            }
        }
    }
    private String buscarDisciplina(){
        String respuesta = String.format("%-10s %-15s", "OPCION", "DISCIPLINA");
        for (int i=0;i<Disciplina.values().length;i++){
            respuesta += String.format("\n%-10s %-15s", i,Disciplina.values()[i]);
        }
        return respuesta;
    }
    private ArrayList<Diagramacion> opcDiagramacionPorDisciplina(Disciplina disciplina, DayOfWeek dia){
        ArrayList<Diagramacion> opcDiagramaciones = new ArrayList<>();
        for (int i=0;i<diagramaciones.size();i++){
            if (diagramaciones.get(i).getDisciplina().equals(disciplina)&&diagramaciones.get(i).getdia().equals(dia)){
                opcDiagramaciones.add(diagramaciones.get(i));
            }
        }
        Comparator<Diagramacion> comparacion = Comparator.comparing(Diagramacion::getdia);
        Collections.sort(opcDiagramaciones,comparacion);
        return opcDiagramaciones;
    }
    private String inscribirAlumno(){
        System.out.println("INGRESE NOMBRE: ");
        sc.nextLine();
        String nombre = sc.nextLine();
        System.out.println("INGRESE APELLIDO: ");
        String apellido = sc.nextLine();
        System.out.println("INGRESE DNI: ");
        String dni = sc.nextLine();
        System.out.println("INGRESE TELEFONO: ");
        String tel = sc.nextLine();
        System.out.println("INGRESE DOMICILIO: ");
        String domicilio = sc.nextLine();
        System.out.println("INGRESE EMAIL: ");
        String email = sc.nextLine();
        System.out.println("Elija una disciplina");
        System.out.println(buscarDisciplina());
        int opcDisciplina = Integer.parseInt(sc.nextLine());
        Disciplina disciplina = Disciplina.values()[opcDisciplina];
        System.out.println("Elija dia de la semana: <<ESCRIBA EL DIA>>");
        System.out.println(Arrays.toString(DayOfWeek.values()));
        DayOfWeek dia = DayOfWeek.valueOf(sc.nextLine());
        System.out.println("Elija la opcion de la diagramacion que prefiera");
        ArrayList<Diagramacion> diaDiagramacion =new ArrayList<>();
        diaDiagramacion = opcDiagramacionPorDisciplina(disciplina,dia);
        System.out.println("Diagramacion para la disciplina "+disciplina+" el dia "+dia+": ");
        System.out.printf("%-5s %-10s %-15s %-8s","OPC","HORARIO","PROFESOR","SALON");
        for (int i=0;i<diaDiagramacion.size();i++){
            System.out.printf("\n%-5s %-33s", i,diaDiagramacion.get(i).tostringPorDisiplicaPorDia());
        }
        int opc3 = Integer.parseInt(sc.nextLine());
        Diagramacion diagramacion = diaDiagramacion.get(opc3);

        return diagramacion.registrarAlumno(nombre,apellido,dni,tel,domicilio,email);
    }

    private String listoAlumnosPorDiagramacion(Profesor profesor, Month mes){
        String  respuesta ="";
        int contAlumnos =0;
        for (int i=0; i<diagramaciones.size();i++){
            if (diagramaciones.get(i).getProfesor().equals(profesor)){
               contAlumnos += diagramaciones.get(i).cuantosAlumnos(mes);
               respuesta += String.format(diagramaciones.get(i).datosDeAlumnos(mes));

            }
        }
        respuesta += String.format("\nTOCARA PAGAR AL PROFESOR "+profesor +" $"+ (contAlumnos*10) + " Pesos por "+contAlumnos+" alumnos.");
        return respuesta;
    }
    private String disciplinaMasElejida(Month mes){
        int[] contAlumnos = new int[Disciplina.values().length];
        for (int i=0; i<diagramaciones.size();i++){
           for(int j=0;j<Disciplina.values().length;j++){
               if (diagramaciones.get(i).getDisciplina().equals(Disciplina.values()[j])){
                   contAlumnos[j] += diagramaciones.get(i).cuantosAlumnos(mes);
               }
           }
        }
        String respuesta ="";
        int max =0;
        boolean igual =false;
        for (int i=0;i<contAlumnos.length;i++) {
            if(i==0&&contAlumnos[i]>0){
                max=contAlumnos[i];
                respuesta = String.valueOf(Disciplina.values()[i]);
            } else if (i>0 && max == contAlumnos[i] ) {
                respuesta += ", "+ String.valueOf(Disciplina.values()[i]);
                igual =true;
            } else if (i>0 && max < contAlumnos[i]) {
                respuesta = String.valueOf(Disciplina.values()[i]);
                max=contAlumnos[i];
                igual = false;
            }
        }
        if (max == 0){
            return "No hay disciplinas vendidas en este mes";
        }else{
            if (igual){
                return "Las diciplinas mas elejidas son: "+respuesta;
            }else {
                return "La diciplina mas elejida es: " + respuesta;
            }
        }
    }
}
