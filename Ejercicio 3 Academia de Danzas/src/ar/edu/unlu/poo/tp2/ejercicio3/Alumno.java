package ar.edu.unlu.poo.tp2.ejercicio3;

import java.time.LocalDate;
import java.time.Month;

public class Alumno {
    private LocalDate fechaInscripcion;
    private String nombre;
    private String apellido;
    private String dni;
    private String tel;
    private String email;
    private String domicilio;

    public Alumno(String nombre, String apellido, String dni, String tel, String domicilio, String email){
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.tel = tel;
        this.email = email;
        this.domicilio = domicilio;
        this.fechaInscripcion = LocalDate.now();
    }
    String datosCredencial(){
        return String.format("%-20s %-15s %-15s %-15s",fechaInscripcion,nombre,apellido,dni);
    }
    Month mesDeInscripcion(){
        return Month.from(fechaInscripcion);
    }
}
