package ar.edu.unlu.poo.tp2.ejercicio3;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;

public class Diagramacion {
    private Disciplina disciplina;
    private Profesor profesor;
    private Salon salon;
    private DayOfWeek dia;
    private LocalTime hora;
    private ArrayList<Alumno> alumnos = new ArrayList<>();
    public Diagramacion(Disciplina disciplina, Profesor profesor, Salon salon, DayOfWeek dia, LocalTime hora){
        this.disciplina = disciplina;
        this.profesor = profesor;
        this.salon = salon;
        this.dia = dia;
        this.hora = hora;
    }

    String generarCredencial(Alumno alumno){
        String respuesta="";
        respuesta += String.format("\n********************************CREDENCIAL********************************");
        respuesta += ("\n<<INSCRIOCION A: "+ this.disciplina+" DIA: "+this.dia+" HORA: "+this.hora+ " SALON: "+this.salon+">>");
        respuesta += String.format("\n%-20s %-15s %-15s %-15s","FECHA INSCRIPCION","NOMBRE","APELLIDO","DNI");
        respuesta += String.format("\n"+alumno.datosCredencial());
        respuesta += String.format("\n********************************CREDENCIAL********************************");
        return respuesta;
    }
    String registrarAlumno(String nombre, String apellido, String dni, String tel, String domicilio, String email){
        Alumno nuevoalumno = new Alumno(nombre,apellido,dni,tel,domicilio,email);
        alumnos.add(nuevoalumno);
        return generarCredencial(nuevoalumno);
    }
    Disciplina getDisciplina(){
        return this.disciplina;
    }
    DayOfWeek getdia(){
        return dia;
    }
    public String tostringPorDisiplicaPorDia(){
        return String.format("%-10s %-15s %-8s",hora,profesor,salon);
    }
    Profesor getProfesor(){
        return this.profesor;
    }
    int cuantosAlumnos(Month mes){
        int cont=0;
        for (int i=0;i<alumnos.size();i++){
            if (alumnos.get(i).mesDeInscripcion().equals(mes)){
                cont ++;
            }
        }
        return cont;
    }
    String datosDeAlumnos(){
        String respuesta ="";
        for (int i=0;i<alumnos.size();i++){
            respuesta += String.format("\n"+alumnos.get(i).datosCredencial());
        }
        return respuesta;
    }
    String datosDeAlumnos(Month mes){
        String respuesta ="";
        for (int i=0;i<alumnos.size();i++){
            if (alumnos.get(i).mesDeInscripcion().equals(mes)) {
                respuesta += String.format("\n" + alumnos.get(i).datosCredencial());
            }
        }
        return respuesta;
    }

}
