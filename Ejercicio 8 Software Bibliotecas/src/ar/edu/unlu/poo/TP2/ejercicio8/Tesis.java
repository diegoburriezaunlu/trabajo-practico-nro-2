package ar.edu.unlu.poo.TP2.ejercicio8;

import java.time.Month;
import java.time.Year;

public class Tesis extends Publicacion {
    private String autor;
    private Year anioDePublicacion;
    private Month mesDePublicacion;

    public Tesis(String nombre, String editor, String telefonoProveedor,
                 String autor, Year anioDePublicacion, Month mesDePublicacion, int stock) {
        super(nombre, editor, telefonoProveedor, stock);
        this.autor = autor;
        this.anioDePublicacion = anioDePublicacion;
        this.mesDePublicacion = mesDePublicacion;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Year getAnioDePublicacion() {
        return anioDePublicacion;
    }

    public void setAnioDePublicacion(Year anioDePublicacion) {
        this.anioDePublicacion = anioDePublicacion;
    }

    public Month getMesDePublicacion() {
        return mesDePublicacion;
    }

    public void setMesDePublicacion(Month mesDePublicacion) {
        this.mesDePublicacion = mesDePublicacion;
    }
}
