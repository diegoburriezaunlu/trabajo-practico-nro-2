package ar.edu.unlu.poo.TP2.ejercicio8;

import java.time.LocalDate;
import java.util.ArrayList;

public abstract class Publicacion {
    private String nombre;
    private String editor;
    private String telefonoProveedor;
    private int stock;
    private int prestados;
    private ArrayList<Prestamo> prestamos = new ArrayList<>();

    public Publicacion(String nombre, String editor, String telefonoProveedor, int stock) {
        this.nombre = nombre;
        this.editor = editor;
        this.telefonoProveedor = telefonoProveedor;
        this.stock = stock;
        this.prestados = 0;
    }
    public boolean prestar(Socio socio){
        if (setPrestados()){
            setPrestamos(socio);
            return true;
        }else{
            return false;
        }
    }
    public ArrayList<Prestamo> getPrestamos() {
        return prestamos;
    }

    public void setPrestamos(Socio socio) {
        Prestamo prestamo = new Prestamo(this, LocalDate.now(),socio);
        this.prestamos.add(prestamo);
    }

    public int getStock() {
        return stock;
    }

    public int getPrestados() {
        return prestados;
    }

    private boolean setPrestados() {
        if (getPrestados()<getStock()){
            this.prestados +=1;
            return true;
        }else{
            return false;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }
}
