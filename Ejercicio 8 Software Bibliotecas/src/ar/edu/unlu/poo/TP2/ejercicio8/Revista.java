package ar.edu.unlu.poo.TP2.ejercicio8;

import java.time.Year;

public class Revista extends Publicacion{
        private String issn;
        private int numeroPublicacion;
        private Year anioDePublicacion;

    public Revista(String nombre, String editor, String telefonoProveedor,
                   String issn, int numeroPublicacion, Year anioDePublicacion, int stock) {
        super(nombre, editor, telefonoProveedor, stock);
        this.issn = issn;
        this.numeroPublicacion = numeroPublicacion;
        this.anioDePublicacion = anioDePublicacion;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public int getNumeroPublicacion() {
        return numeroPublicacion;
    }

    public void setNumeroPublicacion(int numeroPublicacion) {
        this.numeroPublicacion = numeroPublicacion;
    }

    public Year getAnioDePublicacion() {
        return anioDePublicacion;
    }

    public void setAnioDePublicacion(Year anioDePublicacion) {
        this.anioDePublicacion = anioDePublicacion;
    }
}
