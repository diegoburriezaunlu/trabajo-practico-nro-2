package ar.edu.unlu.poo.TP2.ejercicio8;

import java.time.Year;
import java.util.ArrayList;

public class Libro extends Publicacion {
    private String isbn;
    private Year anioDePublicacion;
    private ArrayList<String> autores;

    public Libro(String nombre, String editor, String telefonoProveedor,
                 String isbn, Year anioDePublicacion, ArrayList<String> autores, int stock) {
        super(nombre, editor, telefonoProveedor, stock);
        this.isbn = isbn;
        this.anioDePublicacion = anioDePublicacion;
        this.autores=autores;

    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Year getAnioDePublicacion() {
        return anioDePublicacion;
    }

    public void setAnioDePublicacion(Year anioDePublicacion) {
        this.anioDePublicacion = anioDePublicacion;
    }

    public ArrayList<String> getAutores() {
        return autores;
    }

    public void setAutores(ArrayList<String> autores) {
        this.autores = autores;
    }
}
