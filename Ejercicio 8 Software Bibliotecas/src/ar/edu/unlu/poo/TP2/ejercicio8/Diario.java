package ar.edu.unlu.poo.TP2.ejercicio8;

import java.time.LocalDate;

public class Diario extends Publicacion {
    private LocalDate fechaDePublicacion;

    public Diario(String nombre, String editor, String telefonoProveedor,
                  LocalDate fechaDePublicacion, int stock) {
        super(nombre, editor, telefonoProveedor, stock);
        this.fechaDePublicacion = fechaDePublicacion;
    }

    public LocalDate getFechaDePublicacion() {
        return fechaDePublicacion;
    }

    public void setFechaDePublicacion(LocalDate fechaDePublicacion) {
        this.fechaDePublicacion = fechaDePublicacion;
    }
}
