package ar.edu.unlu.poo.TP2.ejercicio8;

import java.util.ArrayList;

public class Aplicacion {
    private ArrayList<Publicacion> publicaciones = new ArrayList<>();
    private ArrayList<Socio> socios = new ArrayList<>();
    private ArrayList<Prestamo> prestamos = new ArrayList<>();

    public ArrayList<String> ejemplaresPrestados(){
        ArrayList<String> ejemplaresPrestados = new ArrayList<>();
        for (int i=0;i<publicaciones.size();i++){
            for (int j=0;j<publicaciones.get(i).getPrestados();j++){
                ejemplaresPrestados.add(publicaciones.get(i).getNombre());
            }
        }
        return ejemplaresPrestados;
    }
    public ArrayList<Libro> ejemplaresLibrosDisponibles(){
        ArrayList<Libro> librosDisponibles = new ArrayList<>();
        for (int i=0;i<publicaciones.size();i++){
            if (publicaciones.get(i) instanceof Libro){
                if (publicaciones.get(i).getPrestados()<publicaciones.get(i).getStock()){
                    librosDisponibles.add((Libro) publicaciones.get(i));
                }
            }
        }
        return librosDisponibles;
    }
}
