package ar.edu.unlu.poo.TP2.ejercicio8;

import java.time.LocalDate;

public class Prestamo {
    private Publicacion publicacion;
    private LocalDate fechaDePrestamo;
    private LocalDate fechaDeDevolucin;
    private Socio socio;

    public Prestamo(Publicacion publicacion, LocalDate fechaDePrestamo, Socio socio) {
        this.publicacion = publicacion;
        this.fechaDePrestamo = fechaDePrestamo;
        this.socio = socio;
    }

    public void setFechaDeDevolucin(LocalDate fechaDeDevolucin) {
        this.fechaDeDevolucin = fechaDeDevolucin;
    }

    public LocalDate getFechaDeDevolucin() {
        return fechaDeDevolucin;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public LocalDate getFechaDePrestamo() {
        return fechaDePrestamo;
    }
}
