package ar.edu.unlu.poo.tp2.ejercicio6;

public class Expendedor {
    private String descripcion;
    private String codigoInterno;
    private Double ventaTotal;
    private double litrosTotal;

    public double getLitrosTotal() {
        return litrosTotal;
    }

    public void setLitrosTotal(double litrosTotal) {
        this.litrosTotal += litrosTotal;
    }

    public Expendedor(String descripcion, String codigo){
        this.descripcion=descripcion;
        this.codigoInterno=codigo;
        this.ventaTotal =0.0;
        this.litrosTotal=0.0;
    }

    void setVentaTotal (double monto){
        this.ventaTotal += monto;
    }
    Double getVentaTotal(){
        return this.ventaTotal;
    }
    String getCodigoInterno(){
        return this.codigoInterno;
    }
    String getDescripcion(){
        return this.descripcion;
    }
}
