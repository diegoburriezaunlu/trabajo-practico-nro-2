package ar.edu.unlu.poo.tp2.ejercicio6;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;

public class Cliente {
    private String nombre;
    private String patente;
    private ArrayList<AcumuladorVenta> ventasMensuales = new ArrayList<>();
    public Cliente(String nombre, String patente){
        this.nombre = nombre;
        this.patente = patente;
    }
    String getNombre(){
        return this.nombre;
    }
    String getPatente(){
        return this.patente;
    }
    private AcumuladorVenta buscarMesDeCompras(Month mes, Year anio){
        AcumuladorVenta respuesta = null;
        for(int i=0;i<ventasMensuales.size();i++){
            if (ventasMensuales.get(i).getmes().equals(mes)&&ventasMensuales.get(i).getAnio().equals(anio)){
                respuesta = ventasMensuales.get(i);
            }
        }
        if (respuesta==null){
            respuesta = new AcumuladorVenta(LocalDate.now());
        }
        return respuesta;
    }
    void cargarCompra(LocalDate fecha,double monto){
        Month mes = Month.from(fecha);
        Year anio = Year.from(fecha);
        buscarMesDeCompras(mes,anio).sumarValor(monto);
    }
    double getMontoTotalCompras(Month mes, Year anio){
        double res=0;
        for(int i=0;i<ventasMensuales.size();i++){
            if (ventasMensuales.get(i).getmes().equals(mes)&&ventasMensuales.get(i).getAnio().equals(anio)){
                res = ventasMensuales.get(i).getTotal();
            }
        }
        return res;
    }
}
