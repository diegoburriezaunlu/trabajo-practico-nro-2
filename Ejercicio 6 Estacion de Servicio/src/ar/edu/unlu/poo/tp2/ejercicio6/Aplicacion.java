package ar.edu.unlu.poo.tp2.ejercicio6;

import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Aplicacion {
    private ArrayList<Venta> ventas = new ArrayList<>();
    private ArrayList<Cliente> clientes = new ArrayList<>();
    private ArrayList<Producto> productos = new ArrayList<>();
    private ArrayList<Expendedor>expendedores = new ArrayList<>();
    private ArrayList<Vendedor> vendedores = new ArrayList<>();

    public void nuevoProducto(String descipcion, Float valorUnitario){
        String codigoInterno;
        if (!productos.isEmpty()){
            codigoInterno = String.valueOf(1+Integer.parseInt(productos.get(productos.size()-1).getCodigoInterno()));
        }else{
            codigoInterno = String.valueOf(1);
        }
        Producto nuevoProducto = new Producto(descipcion,valorUnitario,codigoInterno);
    }
    public void nuevoExpendedor(String descripcion){
        String codigoInterno;
        if (!expendedores.isEmpty()){
            codigoInterno = String.valueOf(1+Integer.parseInt(expendedores.get(expendedores.size()-1).getCodigoInterno()));
        }else{
            codigoInterno = String.valueOf(1);
        }
        Expendedor nuevoExpendedor = new Expendedor(descripcion,codigoInterno);
    }
    public void nuevoVendedor(String nombre){
        String codigoInterno;
        if (!vendedores.isEmpty()){
            codigoInterno = String.valueOf(1+Integer.parseInt(vendedores.get(vendedores.size()-1).getCodigoInterno()));
        }else{
            codigoInterno = String.valueOf(1);
        }
        Expendedor nuevoExpendedor = new Expendedor(nombre,codigoInterno);
    }
    public String listadoDeProporcionVentaPorExpendedor(){
        double [] cantPorCombustible = new double[productos.size()];
        double sumaTotal=0;
        String respuesta="";
        for (int i=0; i< productos.size();i++){
            for(int j=0;j< ventas.size();j++){
                if (ventas.get(j).getProducto().equals(productos.get(i))){
                    cantPorCombustible [i] += ventas.get(j).getLitros();
                    sumaTotal+= ventas.get(j).getLitros();
                }
            }
        }
        for (int i=0;i< cantPorCombustible.length;i++){
            double cuenta = ((cantPorCombustible[i]*100)/sumaTotal);
            respuesta += String.format("\n Expendedor "+expendedores.get(i).getDescripcion()+" vendio el: "+cuenta+"% del total.");
        }
        return respuesta;
    }
    public String listadoExpendedoresPorVentaTotal(){
        ArrayList<Expendedor> expendedoresOrdenados = new ArrayList<>();
        expendedoresOrdenados.addAll(expendedores);
        Comparator<Expendedor> tipoComparacion = Comparator.comparing(Expendedor::getVentaTotal);
        Collections.sort(expendedoresOrdenados,tipoComparacion);
       String respuesta="";
       for (int i=0;i<expendedoresOrdenados.size();i++){
           respuesta += String.format("El expendedor "+expendedoresOrdenados.get(i).getDescripcion()+" vendio $"+expendedoresOrdenados.get(i).getVentaTotal());
       }
       return respuesta;
    }
    public String listadoExpendedoresPorLitrosVendidos(){
        ArrayList<Expendedor> expendedoresOrdenados = new ArrayList<>();
        expendedoresOrdenados.addAll(expendedores);
        Comparator<Expendedor> tipoComparacion = Comparator.comparing(Expendedor::getLitrosTotal);
        Collections.sort(expendedoresOrdenados,tipoComparacion);
        String respuesta="";
        for (int i=0;i<expendedoresOrdenados.size();i++){
            respuesta += String.format("El expendedor "+expendedoresOrdenados.get(i).getDescripcion()+" vendio "+expendedoresOrdenados.get(i).getLitrosTotal()+" Litros");
        }
        return respuesta;
    }
    public String listadoEmpleadosOrdenadosPorVentasMensual(Month mes, Year anio){
        ArrayList<Vendedor> vendedoresOrdenados = new ArrayList<>();
        ArrayList<Double> montos = new ArrayList<>();
        vendedoresOrdenados.addAll(vendedores);
        for (int i=0;i<vendedoresOrdenados.size();i++){
            montos.add(vendedoresOrdenados.get(i).getMontoTotalVentas(mes,anio));
        }
        for(int i=0;i< vendedoresOrdenados.size();i++){
            for (int j=0; j<vendedoresOrdenados.size();j++){
                if (vendedoresOrdenados.get(j).getMontoTotalVentas(mes,anio)>vendedoresOrdenados.get(j+1).getMontoTotalVentas(mes,anio)){
                    Vendedor aux = vendedoresOrdenados.get(j);
                    Double auxMonto = montos.get(j);
                    vendedoresOrdenados.add(j,vendedoresOrdenados.get(j+1));
                    montos.add(j,montos.get(j+1));
                    vendedoresOrdenados.add(j+1,aux);
                    montos.add(j+1,auxMonto);
                }
            }
        }
        String res="Lista de vendedores en el mes de "+ mes+": ";
        for (int i=0; i<vendedoresOrdenados.size();i++){
            res = String.format("\nVendedor: "+vendedoresOrdenados.get(i).getNombre()+" vendio $"+montos.get(i));
        }
        return res;
    }
    public String listado10ClientesOrdenadosPorCompraMensual(Month mes, Year anio){
        ArrayList<Cliente> clientesOrdenados = new ArrayList<>();
        ArrayList<Double> montos = new ArrayList<>();
        clientesOrdenados.addAll(clientes);
        for (int i=0;i<clientesOrdenados.size();i++){
            montos.add(clientesOrdenados.get(i).getMontoTotalCompras(mes,anio));
        }
        for(int i=0;i< clientesOrdenados.size();i++){
            for (int j=0; j<clientesOrdenados.size();j++){
                if (clientesOrdenados.get(j).getMontoTotalCompras(mes,anio)>clientesOrdenados.get(j+1).getMontoTotalCompras(mes,anio)){
                    Cliente aux = clientesOrdenados.get(j);
                    Double auxMonto = montos.get(j);
                    clientesOrdenados.add(j,clientesOrdenados.get(j+1));
                    montos.add(j,montos.get(j+1));
                    clientesOrdenados.add(j+1,aux);
                    montos.add(j+1,auxMonto);
                }
            }
        }
        String res="Lista de vendedores en el mes de "+ mes+": ";
        int cant=10;
        if (clientesOrdenados.size()<10){
            cant = clientesOrdenados.size();
        }
        for (int i=0; i<cant;i++){
            res = String.format("\nVendedor: "+clientesOrdenados.get(i).getNombre()+" vendio $"+montos.get(i));
        }
        return res;
    }
}
