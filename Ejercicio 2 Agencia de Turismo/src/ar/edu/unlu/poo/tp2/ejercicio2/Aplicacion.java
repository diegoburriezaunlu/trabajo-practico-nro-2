package ar.edu.unlu.poo.tp2.ejercicio2;

import java.time.Month;
import java.util.ArrayList;
import java.util.Scanner;

public class Aplicacion {
    private ArrayList<Transporte> transportes =new ArrayList<>();
    private ArrayList<Hospedaje> hospedajes =new ArrayList<>();
    private ArrayList<Escurcion> escurciones =new ArrayList<>();
    private ArrayList<PaqueteTurismo> paquetes = new ArrayList<>();
    public void menu(){
        //Toda la carga de los objetos en forma automatica al inicio.
        //Cargo transportes.
        Transporte nuevoTransporte = new Transporte(TipoTransporte.AEREO, "Aerolineas Argentinas");
        transportes.add(nuevoTransporte);
        nuevoTransporte = new Transporte(TipoTransporte.AEREO, "Alitalia");
        transportes.add(nuevoTransporte);
        nuevoTransporte = new Transporte(TipoTransporte.MARINA, "Buquebus");
        transportes.add(nuevoTransporte);
        nuevoTransporte = new Transporte(TipoTransporte.MARINA, "Titanic SA");
        transportes.add(nuevoTransporte);
        nuevoTransporte = new Transporte(TipoTransporte.TERRESTRE, "La isleña");
        transportes.add(nuevoTransporte);
        nuevoTransporte = new Transporte(TipoTransporte.TERRESTRE, "Atlantida");
        transportes.add(nuevoTransporte);

        //Cargo hospedajes.
        Hospedaje nuevohospedaje = new Hospedaje(TipoHospedaje.BUNGALO,"Heidi");
        hospedajes.add(nuevohospedaje);
        nuevohospedaje = new Hospedaje(TipoHospedaje.BUNGALO,"La Costa");
        hospedajes.add(nuevohospedaje);
        nuevohospedaje = new Hospedaje(TipoHospedaje.HOSTERIA,"La montaña magica");
        hospedajes.add(nuevohospedaje);
        nuevohospedaje = new Hospedaje(TipoHospedaje.HOSTERIA,"De locos");
        hospedajes.add(nuevohospedaje);
        nuevohospedaje = new Hospedaje(TipoHospedaje.HOTEL,"Playa");
        hospedajes.add(nuevohospedaje);
        nuevohospedaje = new Hospedaje(TipoHospedaje.HOTEL,"Buenas noches");
        hospedajes.add(nuevohospedaje);

        //Cargo Escurciones
        Escurcion nuevaEscurcion = new Escurcion("Juan", "Los lagos");
        escurciones.add(nuevaEscurcion);
        nuevaEscurcion = new Escurcion("Pedro", "La montaña");
        escurciones.add(nuevaEscurcion);
        nuevaEscurcion = new Escurcion("Antonio", "La playa");
        escurciones.add(nuevaEscurcion);
        nuevaEscurcion = new Escurcion("Jose", "En bicicleta");
        escurciones.add(nuevaEscurcion);

        //Cargo paquetes de turismo
            //cargo todos los paquetes *Sin escurciones
            for (int i =0;i<transportes.size();i++) {
                for (int j = 0; j < hospedajes.size(); j++) {
                    for (int k=0;k<Destino.values().length;k++) {
                        PaqueteTurismo paquete = new PaqueteTurismo(transportes.get(i), hospedajes.get(j), "Transporte hospedaje y escurciones", Destino.values()[(int)(Math.random()*(Destino.values().length-1))]);
                        paquetes.add(paquete);
                    }
                }
            }
            //cargo una escurcion a cada paquete en forma aleatoria
            for (int i=0;i<paquetes.size();i++){
                int numero = (int) (Math.random()*(escurciones.size()-1));
                paquetes.get(i).agregarEscurcion(escurciones.get(numero));
            }
            //Elijo en forma aleatoria la mitad de paquetes que hay y le agrego una escurcion aleatoia.
            //De esta manera va a haber varios paquetes con mas de una escurcion.
            for (int i=0;i<(paquetes.size()-(paquetes.size()/2));i++){
                int numero = (int) (Math.random()*(escurciones.size()-1));
                int numero2 = (int) (Math.random()*(paquetes.size()-1));
                paquetes.get(numero2).agregarEscurcion(escurciones.get(numero));
            }
            //Cargo 50 compras de paquetes en forma aleatoria.
            String nombres = "Juan,Pedro,Jose,Damian,Diego,Nicolas,Gonzalo,Antonio,Emilia,Eliana,Nash";
            String[] nombreparts = nombres.split(",");
            String apellidos = "Gomez,Gonzalez,Sanchez,Gutierrez,Lopez,Guerra,Guemes,Belgrano,Garcia,Perez";
            String [] apellidoparts = apellidos.split(",");
            String domicilios= "Belgrano 1519,Mitre 158,Alsina 589,Guemes 412";
            String [] domicilioparts = domicilios.split(",");
            for (int i=0;i<50;i++){
                int num1 = (int) (Math.random()*(paquetes.size()-1));
                paquetes.get(num1).contratacion(nombreparts[(int)(Math.random()*(nombreparts.length-1))],
                        apellidoparts[(int)(Math.random()*(apellidoparts.length-1))],String.valueOf((int)(Math.random()*30000000)),String.valueOf((int)(Math.random()*999999)),
                        domicilioparts[(int)(Math.random()*(domicilioparts.length-1))]);
            }


        Scanner sc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("****************************************************");
            System.out.println("          1. Ventas realizadas por destino");
            System.out.println("          2. Destino favorito");
            System.out.println("          0. Salir");
            System.out.println("****************************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    sc.nextLine();
                    System.out.println("INGRESE EL MES QUE QUIERE LISTAR: ");
                    System.out.println("<<todas las contrataciones estan con fecha de hoy>>");
                    System.out.println("<<Ingrese el mes de 1 al 12>>");
                    int opc2 = Integer.parseInt(String.valueOf(sc.nextLine()));
                    System.out.println(ventasRealizadasPorMes(Month.of(opc2)));
                    sc.nextLine();
                    break;
                case 2:
                    sc.nextLine();
                    System.out.println(destinoFavorito());
                    sc.nextLine();
                    break;
                default:
                    System.out.println("Solo numeros entre 0 y 2");

            }
        }
    }

    private String ventasRealizadasPorMes(Month mes){
        String respuesta = String.format("*******************************************************************************\n");
        respuesta+= String.format("%-10s %-20s %-15s %-15s %-20s","NOMBRE","APELLIDO","DNI","TELEFONO","DOMICILIO");
        respuesta+= String.format("\n*******************************************************************************");
        for (int i=0;i<Destino.values().length;i++){
            respuesta += String.format("\n\n<<<Destino "+Destino.values()[i]+">>>");
            for (int j=0;j<paquetes.size();j++){
                if (paquetes.get(j).esDestino(Destino.values()[i])){
                    respuesta += String.format(paquetes.get(j).ventasMensual(mes));
                }
            }
        }
        respuesta+= String.format("\n*******************************************************************************");
        return respuesta;

    }
    private String destinoFavorito(){
    String respuesta="******************************";
    int[] frecuencia=new int[Destino.values().length];
    for (int i=0;i< Destino.values().length;i++){
        for (int j=0;j< paquetes.size();j++){
            if (paquetes.get(j).esDestino(Destino.values()[i])){
                frecuencia[i]++;
            }
        }
    }
    String eleccion="";
    int cant=0;
    boolean iguales=false;
    for (int i=0; i<frecuencia.length;i++){
        if (i==0){
            eleccion += Destino.values()[i];
            cant = frecuencia[i];
        }else if (frecuencia[i]>cant){
            eleccion = String.valueOf(Destino.values()[i]);
            cant=frecuencia[i];
        }else if (frecuencia[i]==cant && i!=0){
            eleccion += " - "+Destino.values()[i];
            iguales=true;
        }
    }
    if (iguales){
        respuesta += "\nLOS DESTINOS FAVORITOS SON: \n"+eleccion+".";
        respuesta+="\n******************************";
    }else {
        respuesta += "\nEL destino favorito es "+eleccion+".";
        respuesta+="\n******************************";
    }
    return respuesta;
    }


}
