package ar.edu.unlu.poo.tp2.ejercicio2;

import java.time.LocalDate;
import java.time.Month;

public class Contratacion {
    private LocalDate fechaDeContratacion;
    private String nombre;
    private String apellido;
    private String dni;
    private String tel;
    private String domicilio;

    public Contratacion (String nombre, String apellido, String dni, String tel, String domicilio){
        this.fechaDeContratacion = LocalDate.now();
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.tel = tel;
        this.domicilio = domicilio;
    }
    Month obtenerMes(){
        return fechaDeContratacion.getMonth();
    }
    public String tostring(){
        return String.format("%-10s %-20s %-15s %-15s %-20s",nombre, apellido, dni, tel, domicilio);
    }
}

