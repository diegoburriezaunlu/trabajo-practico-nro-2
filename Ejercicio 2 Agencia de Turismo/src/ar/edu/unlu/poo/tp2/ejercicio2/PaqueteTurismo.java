package ar.edu.unlu.poo.tp2.ejercicio2;

import java.time.Month;
import java.util.ArrayList;

public class PaqueteTurismo {
    private Transporte transporte;
    private Hospedaje hospedaje;
    private ArrayList<Escurcion> escurciones =new ArrayList<>();
    private ArrayList<Contratacion> contrataciones=new ArrayList<>();
    private String descripcion;
    private Destino destino;

    public PaqueteTurismo(Transporte transporte, Hospedaje hospedaje, String descripcion, Destino destino){
        this.transporte =transporte;
        this.hospedaje=hospedaje;
        this.descripcion=descripcion;
        this.destino=destino;
    }
    void agregarEscurcion(Escurcion escurcion){
        this.escurciones.add(escurcion);
    }
    void contratacion(String nombre, String apellido, String dni, String tel, String domicilio){
        Contratacion nuevaCompra = new Contratacion(nombre,apellido,dni,tel,domicilio);
        contrataciones.add(nuevaCompra);
    }
    String  ventasMensual(Month mes){
        String respuesta="";
        for (int i=0;i<contrataciones.size();i++){
            if (contrataciones.get(i).obtenerMes().equals(mes)){
                respuesta += "\n"+contrataciones.get(i).tostring();
            }
        }
        return respuesta;
    }
    boolean esDestino(Destino destino){
        if (this.destino.equals(destino)){
            return true;
        }else{
            return false;
        }
    }

}
