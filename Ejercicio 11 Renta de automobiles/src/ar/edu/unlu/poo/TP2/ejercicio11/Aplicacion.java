package ar.edu.unlu.poo.TP2.ejercicio11;

import java.time.LocalDate;
import java.util.ArrayList;

public class Aplicacion {
    private ArrayList<Vehiculo> vehiculos = new ArrayList<>();
    private ArrayList<Cliente> clientes = new ArrayList<>();
    private ArrayList<Presupuesto> presupuestos = new ArrayList<>();

    public void cargarVehiculo(String tipoVehiculo, String patente, int cantAsientos ){
        boolean encontrado = false;
        for (int i =0;i<vehiculos.size();i++){
            if (vehiculos.get(i).getPatente().equals(patente)){
                encontrado = true;
            }
        }
        if (!encontrado) {
            if (tipoVehiculo.equals("Auto")) {
                Auto nuevoAuto = new Auto(cantAsientos, patente);
                vehiculos.add(nuevoAuto);
            }
            if (tipoVehiculo.equals("AutoVip")) {
                AutoVip nuevoAuto = new AutoVip(cantAsientos, patente);
                vehiculos.add(nuevoAuto);
            }
        }
    }
    public void cargarVehiculo(String tipoVehiculo, String patente){
        boolean encontrado = false;
        for (int i =0;i<vehiculos.size();i++){
            if (vehiculos.get(i).getPatente().equals(patente)){
                encontrado = true;
            }
        }
        if (!encontrado) {
            if (tipoVehiculo.equals("Combi")) {
                Combi nuevaCombi = new Combi(patente);
                vehiculos.add(nuevaCombi);
            }
            if (tipoVehiculo.equals("Camioneta")) {
                Camioneta nuevaCamioneta = new Camioneta(patente);
                vehiculos.add(nuevaCamioneta);
            }
            if (tipoVehiculo.equals("Camion")) {
                Camion nuevoCamion = new Camion(patente);
                vehiculos.add(nuevoCamion);
            }
        }
    }
    public Presupuesto calcularPresupuesto(String nombre, String apellido,String dni,
                                           int diasAlquiler,Vehiculo vehiculo){
        Cliente nuevoCliente = new Cliente(nombre, apellido, dni);
        clientes.add(nuevoCliente);
        Presupuesto nuevoPresupuesto = new Presupuesto(nuevoCliente,vehiculo,diasAlquiler);
        presupuestos.add(nuevoPresupuesto);
        return nuevoPresupuesto;
    }
    public void generarAlquiler(Presupuesto presupuesto){
        presupuesto.setFechaAlquiler(LocalDate.now());
    }
    public ArrayList<Presupuesto> alquileresGenerados(Cliente cliente){
        ArrayList<Presupuesto> respuesta = new ArrayList<>();
        for (int i =0; i<presupuestos.size();i++){
            if(presupuestos.get(i).getCliente().equals(cliente)&presupuestos.get(i).isAlquilado()){
                respuesta.add(presupuestos.get(i));
            }
        }
        return respuesta;
    }
    public Double alquileresGenerados(){
        Double respuesta=0.0;
        for (int i =0; i<presupuestos.size();i++){
            if(presupuestos.get(i).isAlquilado()){
                respuesta+=presupuestos.get(i).getValor();
            }
        }
        return respuesta;
    }
}
