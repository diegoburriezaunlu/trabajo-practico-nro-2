package ar.edu.unlu.poo.TP2.ejercicio11;

public interface Vehiculo {
    Double calcularPrecio(int dias);
    String getPatente();
}
