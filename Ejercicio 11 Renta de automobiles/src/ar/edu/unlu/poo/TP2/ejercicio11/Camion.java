package ar.edu.unlu.poo.TP2.ejercicio11;

public class Camion implements Vehiculo{
    private String patente;

    public Double calcularPrecio(int dias){
        if (dias>30){
            return dias*75000.0;
        }else{
            return dias*100000.0;
        }
    }

    public Camion(String patente) {
        this.patente = patente;
    }

    public String getPatente() {
        return patente;
    }
}
