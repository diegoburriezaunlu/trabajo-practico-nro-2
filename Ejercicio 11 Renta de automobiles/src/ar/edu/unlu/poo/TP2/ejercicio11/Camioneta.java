package ar.edu.unlu.poo.TP2.ejercicio11;

public class Camioneta implements Vehiculo{
    private String patente;
    public Double calcularPrecio(int dias){
        return dias*(3000.0+600.00);
    }

    public Camioneta(String patente) {
        this.patente = patente;
    }

    public String getPatente() {
        return patente;
    }
}
