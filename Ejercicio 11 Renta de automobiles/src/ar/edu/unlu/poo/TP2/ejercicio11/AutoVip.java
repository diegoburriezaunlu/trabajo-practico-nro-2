package ar.edu.unlu.poo.TP2.ejercicio11;

public class AutoVip extends Auto{

    public AutoVip(int cantAsientos, String patente) {
        super(cantAsientos, patente);
    }
    public Double calcularPrecio(int dias){
        return dias*(3000.00+(this.getCantAsientos()*500.00));
    }
}
