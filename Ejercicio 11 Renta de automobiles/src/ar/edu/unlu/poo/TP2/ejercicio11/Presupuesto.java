package ar.edu.unlu.poo.TP2.ejercicio11;

import java.time.LocalDate;

public class Presupuesto {
    private LocalDate fechaPresupuesto;
    private Cliente cliente;
    private Vehiculo vehiculo;
    private Double valor;
    private int cantDiasAlquiler;
    private LocalDate fechaAlquiler;
    private LocalDate fechaDevoluvion;
    private boolean alquilado;

    public Presupuesto(Cliente cliente, Vehiculo vehiculo, int cantDiasAlquiler) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.cantDiasAlquiler = cantDiasAlquiler;
        this.fechaPresupuesto = LocalDate.now();
        this.valor = vehiculo.calcularPrecio(cantDiasAlquiler);
        this.alquilado = false;
    }

    public LocalDate getFechaPresupuesto() {
        return fechaPresupuesto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public Double getValor() {
        return valor;
    }

    public int getCantDiasAlquiler() {
        return cantDiasAlquiler;
    }

    public LocalDate getFechaAlquiler() {
        return fechaAlquiler;
    }

    public LocalDate getFechaDevoluvion() {
        return fechaDevoluvion;
    }

    public boolean isAlquilado() {
        return alquilado;
    }

    public void setFechaAlquiler(LocalDate fechaAlquiler) {
        this.fechaAlquiler = fechaAlquiler;
        this.fechaDevoluvion = fechaAlquiler.plusDays(this.cantDiasAlquiler);
        this.alquilado = true;
    }
}
