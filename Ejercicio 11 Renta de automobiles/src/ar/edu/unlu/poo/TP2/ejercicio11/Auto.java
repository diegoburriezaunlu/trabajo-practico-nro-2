package ar.edu.unlu.poo.TP2.ejercicio11;

public class Auto implements Vehiculo {
    private int cantAsientos;
    private String patente;

    public Auto(int cantAsientos, String patente ) {
        this.cantAsientos = cantAsientos;
        this.patente = patente;
    }

    public Double calcularPrecio(int dias){
        return dias*(3000.00+(this.getCantAsientos()*300.00));
    }

    protected int getCantAsientos() {
        return cantAsientos;
    }

    public String getPatente() {
        return patente;
    }
}
