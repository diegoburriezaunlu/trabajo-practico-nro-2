package ar.edu.unlu.poo.TP2.ejercicio11;

public class Combi implements Vehiculo {
    private String patente;
    public Double calcularPrecio(int dias){
        return dias*4500.00;
    }

    public Combi(String patente) {
        this.patente = patente;
    }

    public String getPatente() {
        return patente;
    }
}
